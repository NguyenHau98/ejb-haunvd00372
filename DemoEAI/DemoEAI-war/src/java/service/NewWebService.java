/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.example.entity.Product;
import com.example.session.ProductFacadeLocal;
import com.example.session.UsersFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author hau nguyen
 */
@WebService(serviceName = "NewWebService")
public class NewWebService {

    @EJB
    private ProductFacadeLocal productFacade;

    @EJB
    private UsersFacadeLocal usersFacade;

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    @WebMethod(operationName = "checkLogin")
    public boolean checkLogin(@WebParam(name = "username") String username, @WebParam(name = "password") String password) {
        return usersFacade.checkLogin(username, password);
    }
    
    @WebMethod(operationName = "findAll")
    public List<Product> findAll() {       
        return productFacade.findAll();
    }
    
    @WebMethod(operationName = "create")
    public void create(@WebParam(name ="id") int id ,@WebParam(name ="name") String name ,@WebParam(name ="price") String price ,@WebParam(name ="description") String description){       
        Product product = new Product(id, name, price, description);
        productFacade.create(product);
    }
    
    @WebMethod(operationName = "remove")
    public void remove(@WebParam(name="id") int id) {       
        Product product = new Product();
        product.setId(id);
        productFacade.remove(product);
    }
    
    
    @WebMethod(operationName = "find")
    public Product find(@WebParam(name="id") int id) {       
       return productFacade.find(id);
    }
    
    @WebMethod(operationName = "edit")
    public void edit(@WebParam(name ="id") int id ,@WebParam(name ="name") String name ,@WebParam(name ="price") String price ,@WebParam(name ="description") String description){       
        Product product = new Product(id, name, price, description);
        productFacade.edit(product);
    }
    
     @WebMethod(operationName = "findByName")
    public List<Product> findByName(@WebParam(name="name") String name) {       
       return productFacade.findByName(name);
    }
    
    
    
}
