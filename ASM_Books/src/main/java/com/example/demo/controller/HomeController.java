package com.example.demo.controller;


import com.example.demo.entity.Book;
import com.example.demo.model.BookModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class HomeController {


    @Autowired
    private BookModel bookModel;

    @RequestMapping(path = "/book" , method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("list" , bookModel.findAll());
        return "list";

    }

    @RequestMapping(value = "/book/create", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("book", new Book());
        return "addBook";
    }

    @RequestMapping(value = "/book/{id}/delete", method = RequestMethod.GET)
    public String delete(@PathVariable int id, RedirectAttributes redirect) {
        bookModel.deleteById(id);
        redirect.addFlashAttribute("success", "Deleted contact successfully!");
        return "redirect:/book";
    }


    @RequestMapping(value = "/book/save", method = RequestMethod.POST)
    public String save(@Valid Book book, BindingResult result, RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return "addBook";
        }
        bookModel.save(book);
        redirect.addFlashAttribute("success", "Saved contact successfully!");
        return "redirect:/book";
    }

    @RequestMapping(value = "/book/{id}/edit", method = RequestMethod.GET)
    public String edit(Model model , @PathVariable int id) {
        model.addAttribute("book", bookModel.findById(id));
        return "update";
    }

    @RequestMapping(value = "/book/search", method = RequestMethod.GET)
    public String search(Model model , @RequestParam("name") String name) {
        model.addAttribute("list", bookModel.findName(name));
        return "list";
    }


}
