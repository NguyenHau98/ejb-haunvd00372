package com.example.demo.model;

import com.example.demo.entity.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookModel extends CrudRepository<Book , Integer> {

    List<Book> findAllByName(String name);

    @Query("select u from Book u where u.name = ?1")
    Book findName(String name);

}
